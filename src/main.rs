use std::path::PathBuf;

use clap::Parser;

pub use error::{Error, Result};

#[macro_use]
mod util;
mod error;
mod operators;

#[derive(Debug, Parser)]
#[command(version, author, about)]
pub struct Options {
    #[command(subcommand)]
    command: Command,
}

#[derive(Debug, Parser)]
enum Command {
    /// Run named operator
    Run {
        /// Operator to run
        operator: operators::Operator,
    },
    /// Generate CRDs files
    GenerateCrds {
        /// Where to put CRDs files
        #[arg(default_value = "crds")]
        path: PathBuf,
    },
    /// Register CRDs on K8S cluster
    RegisterCrds,
}

#[tokio::main]
async fn main() -> miette::Result<()> {
    setup_logger();

    let options = Options::parse();

    match options.command {
        Command::Run { operator } => Ok(operator.run().await?),
        Command::GenerateCrds { path } => {
            use std::fs;

            if !path.exists() {
                fs::create_dir_all(&path).expect("Could not create CRDs directory");
            }

            for (name, crd) in operators::Operator::crds() {
                let path = path.join(format!("{name}.yaml"));
                let file = fs::File::create(path).expect("Could not create CRD file");
                serde_yaml::to_writer(file, &crd).expect("Could not encode CRD to file");
            }

            Ok(())
        }
        Command::RegisterCrds => {
            use k8s_openapi::apiextensions_apiserver::pkg::apis::apiextensions::v1 as apiextensions;
            use kube::{
                api::{Patch, PatchParams},
                Api, Client,
            };

            let client = Client::try_default().await.map_err(Error::from)?;
            let crds: Api<apiextensions::CustomResourceDefinition> = Api::all(client.clone());

            for (name, crd) in operators::Operator::crds() {
                crds.patch(name, &PatchParams::apply("polyhedron"), &Patch::Apply(crd))
                    .await
                    .map_err(Error::from)?;
            }

            Ok(())
        }
    }
}

fn setup_logger() {
    use tracing_subscriber::prelude::*;
    use tracing_subscriber::{filter::LevelFilter, fmt, EnvFilter};

    let fmt_layer = fmt::layer().without_time().with_target(false);

    let filter_layer = EnvFilter::builder()
        .with_default_directive(LevelFilter::INFO.into())
        .from_env_lossy();

    tracing_subscriber::registry()
        .with(filter_layer)
        .with(fmt_layer)
        .init();
}
