pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, thiserror::Error, miette::Diagnostic)]
pub enum Error {
    #[error(transparent)]
    #[diagnostic(code(kube))]
    Kube(#[from] kube::Error),
    #[error(transparent)]
    #[diagnostic(code(kube::commit))]
    KubeCommit(#[from] kube::api::entry::CommitError),
}
