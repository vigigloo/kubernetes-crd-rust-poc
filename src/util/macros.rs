macro_rules! btreemap {
    {$(
        $key:expr => $value:expr
    ),*$(,)?} => {{
        let mut map = std::collections::BTreeMap::new();
        $(map.insert($key.into(), $value.into());)*
        map
    }}
}
