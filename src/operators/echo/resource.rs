use k8s_openapi::api::apps::v1 as apps;

#[derive(
    Debug,
    Clone,
    PartialEq,
    serde::Serialize,
    serde::Deserialize,
    schemars::JsonSchema,
    kube::CustomResource,
)]
#[kube(
    group = "vigigloo.fr",
    version = "v1",
    kind = "Echo",
    plural = "echoes",
    status = "EchoStatus",
    scale = r#"{
        "specReplicasPath": ".spec.replicas",
        "statusReplicasPath": ".status.deployment.replicas"
    }"#
)]
#[serde(rename_all = "camelCase")]
pub struct EchoSpec {
    #[validate(range(min = 1))]
    pub replicas: u32,
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize, schemars::JsonSchema)]
#[serde(rename_all = "camelCase")]
pub struct EchoStatus {
    pub deployment: Option<apps::DeploymentStatus>,
}
