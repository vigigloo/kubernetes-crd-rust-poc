use std::time::Duration;

use k8s_openapi::api::apps::v1 as apps;
use kube::{runtime::controller::Action, Api, Resource as _, ResourceExt as _};

use crate::util::resource::ResourceExt as _;
use crate::Result;

pub mod resource;

operator! {
    resource: resource::Echo,

    apply: apply,
}

async fn apply(ctx: ContextRef, res: ResourceRef) -> Result<Action> {
    let deployment = setup_deployment(ctx.clone(), res.clone()).await?;
    update_status(ctx.clone(), res.clone(), &deployment).await?;

    Ok(Action::requeue(Duration::from_secs(5)))
}

async fn setup_deployment(ctx: ContextRef, res: ResourceRef) -> Result<apps::Deployment> {
    use k8s_openapi::api::core::v1 as core;
    use k8s_openapi::apimachinery::pkg::apis::meta::v1::LabelSelector;
    use kube::api::{ObjectMeta, Patch, PatchParams, PostParams};

    let namespace = res.namespace_or_default(&ctx.client);
    let name = res.name_any();

    let deployments = Api::namespaced(ctx.client.clone(), namespace);

    let owner = res.controller_owner_ref(&()).unwrap();

    let labels = btreemap! {
        "app" => name.clone(),
    };

    let deployment = apps::Deployment {
        metadata: ObjectMeta {
            name: Some(name.clone()),
            namespace: Some(namespace.into()),
            labels: Some(labels.clone()),
            owner_references: Some(vec![owner.clone()]),
            ..Default::default()
        },
        spec: Some(apps::DeploymentSpec {
            replicas: Some(res.spec.replicas as i32),
            selector: LabelSelector {
                match_labels: Some(labels.clone()),
                ..Default::default()
            },
            template: core::PodTemplateSpec {
                spec: Some(core::PodSpec {
                    containers: vec![core::Container {
                        name: name.clone(),
                        image: Some("inanimate/echo-server:latest".into()),
                        ports: Some(vec![core::ContainerPort {
                            container_port: 8080,
                            ..Default::default()
                        }]),
                        ..Default::default()
                    }],
                    ..Default::default()
                }),
                metadata: Some(ObjectMeta {
                    labels: Some(labels.clone()),
                    ..Default::default()
                }),
            },
            ..Default::default()
        }),
        ..Default::default()
    };

    let deployment = if deployments.get_metadata_opt(&name).await?.is_some() {
        let params = PatchParams::default();
        deployments
            .patch(
                &name,
                &params,
                &Patch::Merge(serde_json::json!({
                    "spec": deployment.spec,
                })),
            )
            .await?
    } else {
        let params = PostParams::default();
        deployments.create(&params, &deployment).await?
    };

    Ok(deployment)
}

async fn update_status(
    ctx: ContextRef,
    res: ResourceRef,
    deployment: &apps::Deployment,
) -> Result<()> {
    use kube::api::{Patch, PatchParams};

    let name = res.name_any();

    let status = Some(resource::EchoStatus {
        deployment: deployment.status.clone(),
    });

    let params = PatchParams::default();
    ctx.api
        .patch_status(
            &name,
            &params,
            &Patch::Merge(serde_json::json!({
                "status": status,
            })),
        )
        .await?;

    Ok(())
}
