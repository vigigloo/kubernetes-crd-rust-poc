macro_rules! operators {
    (
        $(#[$meta:meta])*
        $name:ident {
            $(
                $(#[$variant_meta:meta])*
                $variant_name:ident => $module:ident
            ),*$(,)?
        }
    ) => {
        $(mod $module;)*

        $(#[$meta])*
        #[derive(Debug, Clone)]
        pub enum $name {
            $(
                $(#[$variant_meta])*
                $variant_name,
            )*
        }

        impl $name {
            pub async fn run(self) -> crate::Result<()> {
                let client = kube::Client::try_default().await?;

                match self {
                    $(Self::$variant_name => {
                        let api = kube::Api::all(client.clone());

                        $module::run(client, api).await
                    })*
                }
            }

            pub fn crds() -> Vec<(
                &'static str,
                k8s_openapi::apiextensions_apiserver::pkg::apis::apiextensions::v1::CustomResourceDefinition,
            )> {
                vec![
                    $((
                        <$module::Resource as kube::CustomResourceExt>::crd_name(),
                        <$module::Resource as kube::CustomResourceExt>::crd(),
                    ),)*
                ]
            }
        }
    };
}

macro_rules! operator {
    {
        resource: $resource_ty:ty,

        apply: $apply:path,
        $(cleanup: $cleanup:path,)?

        $(error_handler: $error_handler:path,)?

        $(
            $(#[$context_meta:meta])*
            context($context_client:ident) {
                $(
                    $(#[$context_field_meta:meta])*
                    $context_field_name:ident: $context_field_ty:ty = $context_field_init:expr
                ),*$(,)?
            }
        )?
    } => {
        pub type Resource = $resource_ty;
        type ResourceRef = std::sync::Arc<Resource>;

        struct Context {
            #[allow(dead_code)]
            client: kube::Client,
            api: kube::Api<Resource>,
            $($(
                $(#[$context_field_meta])*
                $context_field_name: $context_field_ty,
            )*)?
        }

        type ContextRef = std::sync::Arc<Context>;

        pub async fn run(client: kube::Client, api: kube::Api<Resource>) -> crate::Result<()> {
            use futures::StreamExt;
            use kube::runtime::Controller;

            let controller = Controller::new(api.clone(), Default::default()).shutdown_on_signal();

            $($(let ($context_field_name, client) = {
                let $context_client = client;
                ($context_field_init, $context_client)
            };)*)?

            let ctx = ContextRef::new(Context {
                client: client.clone(),
                api,
                $($(
                    $context_field_name,
                )*)?
            });

            let error_handler = None
                $(.or($error_handler))?
                .unwrap_or(super::on_error);

            controller
                .run(reconcile, error_handler, ctx)
                .for_each(|_| futures::future::ready(()))
                .await;

            Ok(())
        }

        #[tracing::instrument(skip(ctx))]
        async fn reconcile(res: ResourceRef, ctx: ContextRef) -> Result<Action, super::Error> {
            use kube::{CustomResourceExt, runtime::finalizer::Event};

            let api = ctx.api.clone();

            let finalizer_name = format!("{}/finalizer", Resource::crd_name());

            kube::runtime::finalizer(
                &api,
                &finalizer_name,
                res,
                |event| async move {
                    match event {
                        Event::Apply(res) => $apply(ctx, res).await,
                        #[allow(unused_variables)]
                        Event::Cleanup(res) => {
                            $(return $cleanup(ctx, res).await;)?
                            #[allow(unreachable_code)]
                            Ok(Action::await_change())
                        },
                    }
                },
            )
            .await
        }
    };
}
